import os
import psycopg2
from psycopg2 import sql
from tabulate import tabulate

url = os.environ.get('DATABASE_URL')

def execute_query(query):
    try:
        connection = psycopg2.connect(url)
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        connection.commit()

        return result

    except Exception as e:
        print(f"Ошибка: {e}")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

query = sql.SQL("""
	SELECT
	    professor.last_name,
	    professor.first_name,
	    professor.middle_name
	FROM
	    student_record
	JOIN
	    professor ON student_record.professor_id = professor.id
	WHERE
	    student_record.student_id = 3;
""")

result = execute_query(query)
print('result:')
print(tabulate(result, headers=["Фамилия", "Имя", "Отчество"], tablefmt="pretty"))

