import os
import ast

def merge_arrays(arr1, arr2):
    result = [item for item in arr1 if item in arr2]
    return result

if __name__ == "__main__":
    input_array1_str = os.environ.get("ARRAY1")
    input_array2_str = os.environ.get("ARRAY2")

    array1 = ast.literal_eval(input_array1_str) if input_array1_str else []
    array2 = ast.literal_eval(input_array2_str) if input_array2_str else []

    result_array = merge_arrays(array1, array2)
    print(result_array)
